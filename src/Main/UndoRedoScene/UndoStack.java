package UndoRedoScene;

import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class UndoStack {

	/**
	 * The undo stack.
	 */
	private Stack<UndoableAction> m_undos;

	/**
	 * The redo stack.
	 */
	private Stack<UndoableAction> m_redos;

	/**
	 * The listeners for empty/refill stack actions.
	 */
	private List<EmptyStackListener> m_emptyStackListeners;

	/**
	 *  The tolerance in terms of difference in time (in ms) for two mergeable actions.
	 */
	private double m_mergeTol;

	/**
	 * Default constructor.
	 * @param mergeTol The merge tolerance (in ms).
	 */
	UndoStack(double mergeTol) {
		m_undos = new Stack<>();
		m_redos = new Stack<>();
		m_emptyStackListeners = new ArrayList<>();
		m_mergeTol = mergeTol;
	}

	/**
	 * Adds a listener for empty/refill stack actions.
	 * @param listener The listener.
	 */
	void addEmptyStackListener(EmptyStackListener listener) {
		m_emptyStackListeners.add(listener);
	}

	/**
	 * Finalizes the action on top of the stack so that it cannot merge with subsequent actions.
	 */
	void finalizeLastAction() {
		if(!m_undos.empty()) {
			UndoableAction prevAction = m_undos.pop();
			prevAction.setFinal();
			m_undos.push(prevAction);
		}
	}

	/**
	 * Adds a new action.
	 * @param action The new action.
	 */
	public void add(UndoableAction action) {
		m_redos.clear();

		if(m_undos.empty() || action.timeSince(m_undos.peek()) > m_mergeTol) {
			m_undos.push(action);
		}
		else {
			UndoableAction prevAction = m_undos.pop();
			boolean merged = prevAction.mergeIn(action);
			m_undos.push(prevAction);
			if(!merged)	m_undos.push(action);
		}

		notifyListeners();
	}

	/**
	 * Undoes a set of related actions.
	 */
	void undo() {
		if(m_undos.empty()) throw new CannotUndoException();

		UndoableAction action = m_undos.pop();
		action.undo();
		m_redos.push(action);

		notifyListeners();
	}

	/**
	 * Redoes a set of related actions.
	 */
	void redo() {
		if(m_redos.empty()) throw new CannotRedoException();

		UndoableAction action = m_redos.pop();
		action.redo();
		m_undos.push(action);

		notifyListeners();
	}

	/**
	 * Clears the undo/redo stacks.
	 */
	void clear() {
		m_undos.clear();
		m_redos.clear();
		notifyListeners();
	}

	/**
	 * Notifies listeners of stack changes.
	 */
	private void notifyListeners() {

		EmptyStackEvent undoEvent = new EmptyStackEvent(
			m_undos, m_undos.empty()
			? EmptyStackEvent.UNDO_EMPTY
			: EmptyStackEvent.UNDO_REFILL
		);
		EmptyStackEvent redoEvent = new EmptyStackEvent(
			m_redos, m_redos.empty()
			? EmptyStackEvent.REDO_EMPTY
			: EmptyStackEvent.REDO_REFILL
		);

		for(EmptyStackListener listener : m_emptyStackListeners) {
			listener.onStackEmpty(undoEvent);
			listener.onStackEmpty(redoEvent);
		}
	}
}
