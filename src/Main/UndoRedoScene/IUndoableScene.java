package UndoRedoScene;

import TPScene.ITPScene;
import UndoRedoScene.UndoableScene.TransformTarget;

import javax.vecmath.Matrix4d;

/**
 * A scene with undoable actions applied to it.
 */
public interface IUndoableScene extends ITPScene {

	/**
	 * Adds a listener for empty/refill stack actions.
	 * @param listener The listener.
	 */
	void addEmptyStackListener(EmptyStackListener listener);

	/**
	 * Apply an undoable transform to the scene.
	 * @param matrix The 4x4 affine transformation matrix.
	 * @param transformTarget The transformation target.
	 * @param id The transform ID that determines how it interacts with other transforms.
	 * @param selectedIndex The index of the selected model.
	 */
	void undoableTransform(
		Matrix4d matrix,
		TransformTarget transformTarget,
		String id,
		int selectedIndex
	);

	/**
	 * Finalizes the previous undoable action so it cannot merge with subsequent actions.
	 */
	void finalizeLastAction();

	/**
	 * Clears the undo/redo stacks.
	 */
	void clearUndoStack();

	/**
	 * Undoes the previous set of related actions.
	 */
	void undo();

	/**
	 * Redoes the previous set of related actions.
	 */
	void redo();
}
