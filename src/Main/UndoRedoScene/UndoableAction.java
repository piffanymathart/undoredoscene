package UndoRedoScene;

import UndoRedoScene.UndoableScene.TransformTarget;

import javax.vecmath.Matrix4d;

/**
 * A transformation action that is undoable.
 */
class UndoableAction {

	/**
	 * The time when the latest action was fired.
	 */
	private double m_time;

	/**
	 * The scene on which the action was performed.
	 */
	private IUndoableScene m_scene;

	/**
	 * The transformation applied.
	 */
	private Matrix4d m_redoTransform;

	/**
	 * The corresponding inverse/undo transformation.
	 */
	private Matrix4d m_undoTransform;

	/**
	 * The transformation target within the scene.
	 */
	private TransformTarget m_transformTarget;

	/**
	 * The action ID that determines how it merges with other actions.
	 */
	private String m_id;

	/**
	 * The index of the selected model.
	 */
	private int m_selectedIndex = -1;

	/**
	 * Whether the action is final and not mergeable with subsequent actions.
	 */
	private boolean m_isFinal;

	/**
	 * Constructor from values.
	 * @param scene The scene to which the action was applied.
	 * @param transform The 4x4 affine transformation matrix.
	 * @param transformTarget The transform target within the scene.
	 * @param id The action ID.
	 * @param selectedIndex The index of the selected model.
	 * @param isFinal Whether the action is final.
	 */
	UndoableAction(
		IUndoableScene scene,
		Matrix4d transform,
		TransformTarget transformTarget,
		String id,
		int selectedIndex,
		boolean isFinal
	) {
		m_time = System.currentTimeMillis();

		m_scene = scene;

		m_redoTransform = new Matrix4d(transform);

		m_undoTransform = new Matrix4d(transform);
		m_undoTransform.invert();

		m_transformTarget = transformTarget;

		m_id = id;

		m_selectedIndex = selectedIndex;

		m_isFinal = isFinal;
	}

	IUndoableScene getScene() {
		return m_scene;
	}

	/**
	 * Merges a new action into the current action.
	 * @param newAction The new action to merge in.
	 * @return Whether the merge is successful.
	 */
	boolean mergeIn(UndoableAction newAction) {

		if(m_transformTarget != newAction.m_transformTarget) return false;
		if(m_transformTarget==TransformTarget.SELECTED && m_selectedIndex != newAction.m_selectedIndex) return false;
		if(!m_id.equals(newAction.m_id)) return false;
		if(m_isFinal) return false;

		Matrix4d newUndoTransform = newAction.m_undoTransform;
		Matrix4d newRedoTransform = newAction.m_redoTransform;

		m_redoTransform.mul(newRedoTransform, m_redoTransform);
		m_undoTransform.mul(m_undoTransform, newUndoTransform);

		m_time = newAction.m_time;

		m_isFinal = newAction.m_isFinal;

		return true;
	}

	/**
	 * Undoes the action.
	 */
	void undo() {
		((UndoableScene) m_scene).transform(m_undoTransform, m_transformTarget, m_selectedIndex);
	}

	/**
	 * Redoes the action.
	 */
	void redo() {
		((UndoableScene) m_scene).transform(m_redoTransform, m_transformTarget, m_selectedIndex);
	}

	/**
	 * Gets the ID of the action.
	 * @return The ID of the action.
	 */
	String getId() { return m_id; }

	/**
	 * Checks whether the action is final.
	 * @return Whether the action is final.
	 */
	boolean isFinal() {
		return m_isFinal;
	}

	/**
	 * Sets the action to final.
	 */
	void setFinal() {
		m_isFinal = true;
	}

	/**
	 * Gets the time elapsed since a previous action.
	 * @param prevAction A previous action.
	 * @return The time elapsed.
	 */
	double timeSince(UndoableAction prevAction) {
		return m_time - prevAction.m_time;
	}
}
