package UndoRedoScene;

/**
 * A listener that listeners to empty/refill stack events
 */
public interface EmptyStackListener {

	/**
	 * Response to an empty/refill stack event.
	 * @param e The empty/refill stack event..
	 */
	void onStackEmpty(EmptyStackEvent e);
}
