package UndoRedoScene;

import java.awt.*;
import java.util.Stack;

/**
 * An event that either empties or refills the undo/redo stack.
 */
public class EmptyStackEvent extends AWTEvent {

	/**
	 * Event ID for undo stack being emptied.
	 */
	public static final int UNDO_EMPTY = AWTEvent.RESERVED_ID_MAX + 1;

	/**
	 * Event ID for undo stack being refilled.
	 */
	public static final int UNDO_REFILL = AWTEvent.RESERVED_ID_MAX + 2;

	/**
	 * Event ID for redo stack being emptied.
	 */
	public static final int REDO_EMPTY = AWTEvent.RESERVED_ID_MAX + 3;

	/**
	 * Event ID for redo stack being refilled.
	 */
	public static final int REDO_REFILL = AWTEvent.RESERVED_ID_MAX + 4;

	/**
	 * Default constructor.
	 * @param src Source of event firing.
	 * @param id Event ID.
	 */
	EmptyStackEvent(Stack src, int id) {
		super(src, id);
	}
}
