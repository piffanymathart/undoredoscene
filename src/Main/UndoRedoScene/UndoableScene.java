package UndoRedoScene;

import Polyhedron.IColouredPolyhedron;
import TPScene.ITPScene;
import TPScene.TPScene;

import javax.vecmath.Matrix4d;

/**
 * A scene with undoable actions applied to it.
 */
public class UndoableScene extends TPScene implements IUndoableScene {

	/**
	 * The undo/redo stacks.
	 */
	private UndoStack m_undoStack;

	/**
	 * The transformation target types.
	 */
	public enum TransformTarget {
		SCENE,
		SCREEN,
		MODELS,
		SELECTED
	}

	/**
	 * Default constructor.
	 * @param scene The underlying scene.
	 */
	public UndoableScene(ITPScene scene) {
		super(scene.getSurface(), scene.getModels(), scene.getFovY(), scene.getTargetMethod());
		m_undoStack = new UndoStack(1000);
	}

	/**
	 * Adds a listener for empty/refill stack actions.
	 * @param listener The listener.
	 */
	public void addEmptyStackListener(EmptyStackListener listener) {
		m_undoStack.addEmptyStackListener(listener);
	}

	/**
	 * Apply a non-undoable transform to the scene.
	 * @param matrix The 4x4 affine transformation matrix.
	 * @param transformTarget The transformation target.
	 * @param selectedIndex The index of the selected model.
	 */
	void transform(
		Matrix4d matrix,
		TransformTarget transformTarget,
		int selectedIndex
	) {
		switch(transformTarget) {
			case SCENE: {
				super.transform(matrix);
				break;
			}
			case SCREEN: {
				transformScreen(matrix);
				break;
			}
			case MODELS: {
				transformModels(matrix);
				break;
			}
			case SELECTED: {
				transformSelected(matrix, selectedIndex);
				break;
			}
		}
	}

	/**
	 * Apply an undoable transform to the scene.
	 * @param matrix The 4x4 affine transformation matrix.
	 * @param transformTarget The transformation target.
	 * @param id The transform ID that determines how it interacts with other transforms.
	 * @param selectedIndex The index of the selected model.
	 */
	public void undoableTransform(
		Matrix4d matrix,
		TransformTarget transformTarget,
		String id,
		int selectedIndex
	) {
		transform(matrix, transformTarget, selectedIndex);

		m_undoStack.add(
			new UndoableAction(
				this,
				matrix,
				transformTarget,
				id,
				selectedIndex,
				false
			)
		);
	}

	/**
	 * Finalizes the previous undoable action so it cannot merge with subsequent actions.
	 */
	public void finalizeLastAction() {
		m_undoStack.finalizeLastAction();
	}

	/**
	 * Clears the undo/redo stacks.
	 */
	public void clearUndoStack() {
		m_undoStack.clear();
	}

	/**
	 * Undoes the previous set of related actions.
	 */
	public void undo() {
		m_undoStack.undo();
	}

	/**
	 * Redoes the previous set of related actions.
	 */
	public void redo() {
		m_undoStack.redo();
	}

	/**
	 * Transforms the screen.
	 * @param matrix The 4x4 affine transformation matrix.
	 */
	private void transformScreen(Matrix4d matrix) {
		// can't just transform the surface since the projections need to be transformed too
		IColouredPolyhedron[] models = getModels();
		// transform entire scene
		super.transform(matrix);
		// set models back to untransformed models so that only the screen is transformed
		for(int i=0; i<models.length; i++) setModel(i, models[i]);
	}

	/**
	 * Transforms the models.
	 * @param matrix The 4x4 affine transformation matrix.
	 */
	private void transformModels(Matrix4d matrix) {
		for(int i=0; i<m_models.length; i++) m_models[i].transform(matrix);
	}

	/**
	 * Transforms the selected model.
	 * @param matrix The 4x4 affine transformation matrix.
	 * @param selectedIndex The index of the selected model.
	 */
	private void transformSelected(Matrix4d matrix, int selectedIndex) {
		m_models[selectedIndex].transform(matrix);
	}
}
