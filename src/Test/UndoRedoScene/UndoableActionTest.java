package UndoRedoScene;

import Polyhedron.IColouredPolyhedron;
import PolyhedronBuilder.PolyhedronBuilder;
import Scene.IScene;
import Scene.ISurface;
import Scene.Surface;
import TPScene.ITPScene;
import TPScene.TPScene;
import Unfolding.IUnfoldablePolyhedron;
import org.junit.jupiter.api.Test;

import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;

import static TPScene.TPScene.TargetMethod.TARGET_ALL;
import static UndoRedoScene.UndoableScene.TransformTarget.*;
import static org.junit.jupiter.api.Assertions.*;

class UndoableActionTest {

	@Test
	void setters_getters() {

		String actionId = "some-id";

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(1,2,3));

		UndoableAction action = new UndoableAction(createUndoableScene(), mat, SCENE, actionId, 1, false);

		assertEquals(actionId, action.getId());
		assertFalse(action.isFinal());
		action.setFinal();
		assertTrue(action.isFinal());
	}

	@Test
	void timeSince() {

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(1,2,3));

		double outerStart = System.currentTimeMillis();

		UndoableAction action1 = new UndoableAction(createUndoableScene(), mat, SCENE, "some-id", 1, false);

		double innerStart = System.currentTimeMillis();
		double innerEnd = System.currentTimeMillis();

		UndoableAction action2 = new UndoableAction(createUndoableScene(), mat, SCENE, "some-id", 1, false);

		double outerEnd = System.currentTimeMillis();

		double timeElapsed = action2.timeSince(action1);
		assert(timeElapsed > innerEnd - innerStart);
		assert(timeElapsed < outerEnd - outerStart);
	}

	@Test
	void redo_targetScene() {

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(1,2,3));

		UndoableAction action = new UndoableAction(createUndoableScene(), mat, SCENE, "some-id", 1, false);
		action.redo();

		IScene expected = createTPScene();
		expected.transform(mat);

		IUndoableScene actual = action.getScene();

		assertEquals(expected.getSurface(), actual.getSurface());
		assertArrayEquals(expected.getModels(), actual.getModels());
		assertEquals(expected.getFovY(), actual.getFovY());
	}

	@Test
	void redo_targetScreen() {

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(1,2,3));

		UndoableAction action = new UndoableAction(createUndoableScene(), mat, SCREEN, "some-id", 1, false);
		action.redo();

		IScene expected = createTPScene();
		ISurface surface = expected.getSurface();
		surface.transform(mat);

		IUndoableScene actual = action.getScene();

		assertEquals(surface, actual.getSurface());
		assertArrayEquals(expected.getModels(), actual.getModels());
		assertEquals(expected.getFovY(), actual.getFovY());
	}

	@Test
	void redo_targetModels() {

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(1,2,3));

		UndoableAction action = new UndoableAction(createUndoableScene(), mat, MODELS, "some-id", 1, false);
		action.redo();

		IScene expected = createTPScene();
		IColouredPolyhedron[] models = expected.getModels();
		for(IColouredPolyhedron model : models) model.transform(mat);

		IUndoableScene actual = action.getScene();

		assertEquals(expected.getSurface(), actual.getSurface());
		assertArrayEquals(models, actual.getModels());
		assertEquals(expected.getFovY(), actual.getFovY());
	}

	@Test
	void redo_targetSelected() {

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(1,2,3));

		int selected = 1;

		UndoableAction action = new UndoableAction(createUndoableScene(), mat, SELECTED, "some-id", selected, false);
		action.redo();

		IScene expected = createTPScene();
		IColouredPolyhedron[] models = expected.getModels();
		models[selected].transform(mat);

		IUndoableScene actual = action.getScene();

		assertEquals(expected.getSurface(), actual.getSurface());
		assertArrayEquals(models, actual.getModels());
		assertEquals(expected.getFovY(), actual.getFovY());
	}

	@Test
	void undo_targetScene() {

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(1,2,3));

		Matrix4d invMat = new Matrix4d();
		invMat.setIdentity();
		invMat.setTranslation(new Vector3d(-1,-2,-3));

		UndoableAction action = new UndoableAction(createUndoableScene(), mat, SCENE, "some-id", 1, false);
		action.undo();

		IScene expected = createTPScene();
		expected.transform(invMat);

		IUndoableScene actual = action.getScene();

		assertEquals(expected.getSurface(), actual.getSurface());
		assertArrayEquals(expected.getModels(), actual.getModels());
		assertEquals(expected.getFovY(), actual.getFovY());
	}

	@Test
	void undo_targetScreen() {

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(1,2,3));

		Matrix4d invMat = new Matrix4d();
		invMat.setIdentity();
		invMat.setTranslation(new Vector3d(-1,-2,-3));

		UndoableAction action = new UndoableAction(createUndoableScene(), mat, SCREEN, "some-id", 1, false);
		action.undo();

		IScene expected = createTPScene();
		ISurface surface = expected.getSurface();
		surface.transform(invMat);

		IUndoableScene actual = action.getScene();

		assertEquals(surface, actual.getSurface());
		assertArrayEquals(expected.getModels(), actual.getModels());
		assertEquals(expected.getFovY(), actual.getFovY());
	}

	@Test
	void undo_targetModels() {

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(1,2,3));

		Matrix4d invMat = new Matrix4d();
		invMat.setIdentity();
		invMat.setTranslation(new Vector3d(-1,-2,-3));

		UndoableAction action = new UndoableAction(createUndoableScene(), mat, MODELS, "some-id", 1, false);
		action.undo();

		IScene expected = createTPScene();
		IColouredPolyhedron[] models = expected.getModels();
		for(IColouredPolyhedron model : models) model.transform(invMat);

		IUndoableScene actual = action.getScene();

		assertEquals(expected.getSurface(), actual.getSurface());
		assertArrayEquals(models, actual.getModels());
		assertEquals(expected.getFovY(), actual.getFovY());
	}

	@Test
	void undo_targetSelected() {

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(1,2,3));

		Matrix4d invMat = new Matrix4d();
		invMat.setIdentity();
		invMat.setTranslation(new Vector3d(-1,-2,-3));

		int selected = 1;

		UndoableAction action = new UndoableAction(createUndoableScene(), mat, SELECTED, "some-id", selected, false);
		action.undo();

		IScene expected = createTPScene();
		IColouredPolyhedron[] models = expected.getModels();
		models[selected].transform(invMat);

		IUndoableScene actual = action.getScene();

		assertEquals(expected.getSurface(), actual.getSurface());
		assertArrayEquals(models, actual.getModels());
		assertEquals(expected.getFovY(), actual.getFovY());
	}

	@Test
	void mergeIn_canMerge() {

		Matrix4d mat1 = new Matrix4d();
		mat1.setIdentity();
		mat1.setTranslation(new Vector3d(1,2,3));

		Matrix4d mat2 = new Matrix4d();
		mat2.setIdentity();
		mat2.setScale(4);

		int selected = 1;

		UndoableAction action1 = new UndoableAction(createUndoableScene(), mat1, SCENE, "some-id", selected, false);
		UndoableAction action2 = new UndoableAction(createUndoableScene(), mat2, SCENE, "some-id", selected, false);

		assertTrue(action1.mergeIn(action2));
		action1.redo();

		IScene expected = createTPScene();
		expected.transform(mat1);
		expected.transform(mat2);

		IUndoableScene actual = action1.getScene();

		assertEquals(expected.getSurface(), actual.getSurface());
		assertArrayEquals(expected.getModels(), actual.getModels());
		assertEquals(expected.getFovY(), actual.getFovY());
	}

	@Test
	void mergeIn_final() {

		Matrix4d mat1 = new Matrix4d();
		mat1.setIdentity();
		mat1.setTranslation(new Vector3d(1,2,3));

		Matrix4d mat2 = new Matrix4d();
		mat2.setIdentity();
		mat2.setScale(4);

		int selected = 1;

		UndoableAction action1 = new UndoableAction(createUndoableScene(), mat1, SCENE, "some-id", selected, true);
		UndoableAction action2 = new UndoableAction(createUndoableScene(), mat2, SCENE, "some-id", selected, false);

		assertFalse(action1.mergeIn(action2));
	}

	@Test
	void mergeIn_differentIds() {

		Matrix4d mat1 = new Matrix4d();
		mat1.setIdentity();
		mat1.setTranslation(new Vector3d(1,2,3));

		Matrix4d mat2 = new Matrix4d();
		mat2.setIdentity();
		mat2.setScale(4);

		int selected = 1;

		UndoableAction action1 = new UndoableAction(createUndoableScene(), mat1, SCENE, "some-id", selected, false);
		UndoableAction action2 = new UndoableAction(createUndoableScene(), mat2, SCENE, "another-id", selected, false);

		assertFalse(action1.mergeIn(action2));
	}

	@Test
	void mergeIn_differentTargets() {

		Matrix4d mat1 = new Matrix4d();
		mat1.setIdentity();
		mat1.setTranslation(new Vector3d(1,2,3));

		Matrix4d mat2 = new Matrix4d();
		mat2.setIdentity();
		mat2.setScale(4);

		int selected = 1;

		UndoableAction action1 = new UndoableAction(createUndoableScene(), mat1, SCENE, "some-id", selected, false);
		UndoableAction action2 = new UndoableAction(createUndoableScene(), mat2, MODELS, "some-id", selected, false);

		assertFalse(action1.mergeIn(action2));
	}

	@Test
	void mergeIn_differentSelectedIndex() {

		Matrix4d mat1 = new Matrix4d();
		mat1.setIdentity();
		mat1.setTranslation(new Vector3d(1,2,3));

		Matrix4d mat2 = new Matrix4d();
		mat2.setIdentity();
		mat2.setScale(4);

		UndoableAction action1 = new UndoableAction(createUndoableScene(), mat1, SELECTED, "some-id", 0, false);
		UndoableAction action2 = new UndoableAction(createUndoableScene(), mat2, SELECTED, "some-id", 1, false);

		assertFalse(action1.mergeIn(action2));
	}

	private ITPScene createTPScene() {

		IUnfoldablePolyhedron cube = PolyhedronBuilder.getCube(true);
		ISurface surface = new Surface(cube);

		IColouredPolyhedron[] models = {
			PolyhedronBuilder.getPyramid(false),
			PolyhedronBuilder.getCube(false)
		};

		double fovY = Math.PI/4.0;

		return new TPScene(surface, models, fovY, TARGET_ALL);
	}

	private IUndoableScene createUndoableScene() {

		return new UndoableScene(createTPScene());
	}
}