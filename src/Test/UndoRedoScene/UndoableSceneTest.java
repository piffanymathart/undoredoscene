package UndoRedoScene;

import Polyhedron.IColouredPolyhedron;
import PolyhedronBuilder.PolyhedronBuilder;
import Scene.IScene;
import Scene.ISurface;
import Scene.Surface;
import TPScene.ITPScene;
import TPScene.TPScene;
import Unfolding.IUnfoldablePolyhedron;
import org.junit.jupiter.api.Test;

import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;

import static TPScene.TPScene.TargetMethod.TARGET_ALL;
import static UndoRedoScene.UndoableScene.TransformTarget.SCENE;
import static org.junit.jupiter.api.Assertions.*;

class UndoableSceneTest {

	@Test
	void transform() {

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(1,2,3));

		IUndoableScene scene = createUndoableScene();
		((UndoableScene) scene).transform(mat, SCENE, 0);

		assertThrows(CannotUndoException.class, () -> scene.undo());
	}

	@Test
	void undoableTransform() {

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(1,2,3));

		IUndoableScene scene = createUndoableScene();
		scene.undoableTransform(mat, SCENE, "some-id", 0);

		scene.undo();

		IScene expected = createTPScene();

		assertEquals(expected.getSurface(), scene.getSurface());
		assertArrayEquals(expected.getModels(), scene.getModels());
		assertEquals(expected.getFovY(), scene.getFovY());
	}

	@Test
	void multipleTransforms() {

		// create 4 transforms

		Matrix4d mat1 = new Matrix4d();
		mat1.setIdentity();
		mat1.setTranslation(new Vector3d(1,2,3));

		Matrix4d mat2 = new Matrix4d();
		mat2.setIdentity();
		mat2.setScale(4);

		Matrix4d mat3 = new Matrix4d();
		mat3.setIdentity();
		mat3.rotX(55);

		Matrix4d mat4 = new Matrix4d();
		mat4.setIdentity();
		mat4.rotX(55);

		// apply them to a scene

		IUndoableScene scene = createUndoableScene();
		scene.undoableTransform(mat1, SCENE, "id1", 0);
		scene.undoableTransform(mat2, SCENE, "id2", 0);
		scene.undoableTransform(mat3, SCENE, "id2", 0);
		scene.undoableTransform(mat4, SCENE, "id3", 0);

		// before any undos, the scene should have all 4 transforms applied to it

		IScene expected = createTPScene();
		expected.transform(mat1);
		expected.transform(mat2);
		expected.transform(mat3);
		expected.transform(mat4);

		assertEquals(expected.getSurface(), scene.getSurface());
		assertArrayEquals(expected.getModels(), scene.getModels());
		assertEquals(expected.getFovY(), scene.getFovY());

		// after one undo, the scene should have transforms 1, 2, 3 applied to it

		scene.undo();

		expected = createTPScene();
		expected.transform(mat1);
		expected.transform(mat2);
		expected.transform(mat3);

		assertEquals(expected.getSurface(), scene.getSurface());
		assertArrayEquals(expected.getModels(), scene.getModels());
		assertEquals(expected.getFovY(), scene.getFovY());

		// after a second undo, transform 2 and 3 are undone, and transform 1 remains

		scene.undo();

		expected = createTPScene();
		expected.transform(mat1);

		assertEquals(expected.getSurface(), scene.getSurface());
		assertArrayEquals(expected.getModels(), scene.getModels());
		assertEquals(expected.getFovY(), scene.getFovY());

		// after a third undo, all transforms are undone

		scene.undo();

		expected = createTPScene();

		assertEquals(expected.getSurface(), scene.getSurface());
		assertArrayEquals(expected.getModels(), scene.getModels());
		assertEquals(expected.getFovY(), scene.getFovY());

		// a fourth undo cannot be done

		assertThrows(CannotUndoException.class, () -> scene.undo());

		// first redo

		scene.redo();

		expected = createTPScene();
		expected.transform(mat1);

		assertEquals(expected.getSurface(), scene.getSurface());
		assertArrayEquals(expected.getModels(), scene.getModels());
		assertEquals(expected.getFovY(), scene.getFovY());

		// second redo

		scene.redo();

		expected = createTPScene();
		expected.transform(mat1);
		expected.transform(mat2);
		expected.transform(mat3);

		assertEquals(expected.getSurface(), scene.getSurface());
		assertArrayEquals(expected.getModels(), scene.getModels());
		assertEquals(expected.getFovY(), scene.getFovY());

		// apply another transform, then cannot redo

		scene.undoableTransform(mat1, SCENE, "id1", 0);

		assertThrows(CannotRedoException.class, () -> scene.redo());

		// another undo

		scene.undo();

		expected = createTPScene();
		expected.transform(mat1);
		expected.transform(mat2);
		expected.transform(mat3);

		assertEquals(expected.getSurface(), scene.getSurface());
		assertArrayEquals(expected.getModels(), scene.getModels());
		assertEquals(expected.getFovY(), scene.getFovY());
	}

	private ITPScene createTPScene() {

		IUnfoldablePolyhedron cube = PolyhedronBuilder.getCube(true);
		ISurface surface = new Surface(cube);

		IColouredPolyhedron[] models = {
			PolyhedronBuilder.getPyramid(false),
			PolyhedronBuilder.getCube(false)
		};

		double fovY = Math.PI/4.0;

		return new TPScene(surface, models, fovY, TARGET_ALL);
	}

	private IUndoableScene createUndoableScene() {

		return new UndoableScene(createTPScene());
	}

}